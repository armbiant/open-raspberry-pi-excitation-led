<div align="center">
<p>
    <a href="https://benchling.com/organizations/acubesat/">Benchling 🎐🧬</a> &bull;
    <a href="https://gitlab.com/acubesat/documentation/cdr-public/-/blob/master/DDJF/DDJF_PL.pdf?expanded=true&viewer=rich">DDJF_PL 📚🧪</a> &bull;
    <a href="https://spacedot.gr/">SpaceDot 🌌🪐</a> &bull;
    <a href="https://acubesat.spacedot.gr/">AcubeSAT 🛰️🌎</a>
</p>
</div>

---

## Description

A repository to host the schematic files for the LED ring-shaped PCB. This PCB will host LEDs mounted on top, and they will shed blue light uniformly onto the PDMS chip culturing the yeast cells, in order for them to fluoresce. The fluorescence readout is then subsequently captured by our in-house imaging system, and used to probe changes in gene expression

![](led_pcb.png)

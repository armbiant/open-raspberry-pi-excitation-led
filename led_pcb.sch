EESchema Schematic File Version 4
LIBS:led_pcb-cache
EELAYER 30 0
EELAYER END
$Descr User 8268 5827
encoding utf-8
Sheet 1 1
Title "Excitation LED Board"
Date "2020-09-04"
Rev "v1.0"
Comp "Aristotle Space & Aeronautics Team (ASAT)"
Comment1 "AcubeSAT"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Components:MLEBLU-A1-0000-000T01 LED1
U 1 1 5F53CFC3
P 1200 1200
F 0 "LED1" H 1700 927 50  0000 C CNN
F 1 "MLEBLU-A1-0000-000T01" H 1700 836 50  0000 C CNN
F 2 "Components:MLEBLUA10000000T01" H 2050 1500 50  0001 L CNN
F 3 "https://www.cree.com/led-components/media/documents/XlampMLE.pdf" H 2050 1400 50  0001 L CNN
F 4 "Cree,LED,MLEBLU-A1-0000-000T01" H 2050 1300 50  0001 L CNN "Description"
F 5 "1.2" H 2050 1200 50  0001 L CNN "Height"
F 6 "941-MLEBLUA10T01" H 2050 1100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Cree-Inc/MLEBLU-A1-0000-000T01?qs=lgxGVv3Zr3IVBDvhb3JFtA%3D%3D" H 2050 1000 50  0001 L CNN "Mouser Price/Stock"
F 8 "Cree, Inc." H 2050 900 50  0001 L CNN "Manufacturer_Name"
F 9 "MLEBLU-A1-0000-000T01" H 2050 800 50  0001 L CNN "Manufacturer_Part_Number"
	1    1200 1200
	1    0    0    -1  
$EndComp
$Comp
L Components:MLEBLU-A1-0000-000T01 LED2
U 1 1 5F53D73C
P 1200 2400
F 0 "LED2" H 1700 2127 50  0000 C CNN
F 1 "MLEBLU-A1-0000-000T01" H 1700 2036 50  0000 C CNN
F 2 "Components:MLEBLUA10000000T01" H 2050 2700 50  0001 L CNN
F 3 "https://www.cree.com/led-components/media/documents/XlampMLE.pdf" H 2050 2600 50  0001 L CNN
F 4 "Cree,LED,MLEBLU-A1-0000-000T01" H 2050 2500 50  0001 L CNN "Description"
F 5 "1.2" H 2050 2400 50  0001 L CNN "Height"
F 6 "941-MLEBLUA10T01" H 2050 2300 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Cree-Inc/MLEBLU-A1-0000-000T01?qs=lgxGVv3Zr3IVBDvhb3JFtA%3D%3D" H 2050 2200 50  0001 L CNN "Mouser Price/Stock"
F 8 "Cree, Inc." H 2050 2100 50  0001 L CNN "Manufacturer_Name"
F 9 "MLEBLU-A1-0000-000T01" H 2050 2000 50  0001 L CNN "Manufacturer_Part_Number"
	1    1200 2400
	1    0    0    -1  
$EndComp
$Comp
L Components:MLEBLU-A1-0000-000T01 LED3
U 1 1 5F53E1F9
P 1200 3550
F 0 "LED3" H 1700 3277 50  0000 C CNN
F 1 "MLEBLU-A1-0000-000T01" H 1700 3186 50  0000 C CNN
F 2 "Components:MLEBLUA10000000T01" H 2050 3850 50  0001 L CNN
F 3 "https://www.cree.com/led-components/media/documents/XlampMLE.pdf" H 2050 3750 50  0001 L CNN
F 4 "Cree,LED,MLEBLU-A1-0000-000T01" H 2050 3650 50  0001 L CNN "Description"
F 5 "1.2" H 2050 3550 50  0001 L CNN "Height"
F 6 "941-MLEBLUA10T01" H 2050 3450 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Cree-Inc/MLEBLU-A1-0000-000T01?qs=lgxGVv3Zr3IVBDvhb3JFtA%3D%3D" H 2050 3350 50  0001 L CNN "Mouser Price/Stock"
F 8 "Cree, Inc." H 2050 3250 50  0001 L CNN "Manufacturer_Name"
F 9 "MLEBLU-A1-0000-000T01" H 2050 3150 50  0001 L CNN "Manufacturer_Part_Number"
	1    1200 3550
	1    0    0    -1  
$EndComp
$Comp
L Components:MLEBLU-A1-0000-000T01 LED4
U 1 1 5F53ECF8
P 1200 4700
F 0 "LED4" H 1700 4427 50  0000 C CNN
F 1 "MLEBLU-A1-0000-000T01" H 1700 4336 50  0000 C CNN
F 2 "Components:MLEBLUA10000000T01" H 2050 5000 50  0001 L CNN
F 3 "https://www.cree.com/led-components/media/documents/XlampMLE.pdf" H 2050 4900 50  0001 L CNN
F 4 "Cree,LED,MLEBLU-A1-0000-000T01" H 2050 4800 50  0001 L CNN "Description"
F 5 "1.2" H 2050 4700 50  0001 L CNN "Height"
F 6 "941-MLEBLUA10T01" H 2050 4600 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Cree-Inc/MLEBLU-A1-0000-000T01?qs=lgxGVv3Zr3IVBDvhb3JFtA%3D%3D" H 2050 4500 50  0001 L CNN "Mouser Price/Stock"
F 8 "Cree, Inc." H 2050 4400 50  0001 L CNN "Manufacturer_Name"
F 9 "MLEBLU-A1-0000-000T01" H 2050 4300 50  0001 L CNN "Manufacturer_Part_Number"
	1    1200 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 1200 1200 1250
Wire Wire Line
	1200 2400 1200 2450
Wire Wire Line
	1200 4700 1200 4750
Text Label 4400 2200 2    50   ~ 0
LED_CONTROL_3
Text Label 4400 1900 2    50   ~ 0
LED_CONTROL_4
Wire Wire Line
	1200 1250 1100 1250
Connection ~ 1200 1250
Wire Wire Line
	1200 1250 1200 1300
Wire Wire Line
	1200 2450 1100 2450
Connection ~ 1200 2450
Wire Wire Line
	1200 2450 1200 2500
Wire Wire Line
	1200 3550 1200 3600
Wire Wire Line
	1200 3600 1100 3600
Connection ~ 1200 3600
Wire Wire Line
	1200 3600 1200 3650
Wire Wire Line
	1200 4750 1100 4750
Connection ~ 1200 4750
Wire Wire Line
	1200 4750 1200 4800
Text Label 1100 1250 2    50   ~ 0
LED_3V3_1
Text Label 1100 2450 2    50   ~ 0
LED_3V3_2
Text Label 1100 3600 2    50   ~ 0
LED_3V3_3
Text Label 1100 4750 2    50   ~ 0
LED_3V3_4
$Comp
L Device:R R2
U 1 1 5F550C99
P 4200 3150
F 0 "R2" V 3993 3150 50  0000 C CNN
F 1 "R" V 4084 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 4130 3150 50  0001 C CNN
F 3 "~" H 4200 3150 50  0001 C CNN
	1    4200 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5F5515BB
P 3600 3150
F 0 "R1" V 3393 3150 50  0000 C CNN
F 1 "R" V 3484 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 3530 3150 50  0001 C CNN
F 3 "~" H 3600 3150 50  0001 C CNN
	1    3600 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5F552129
P 4800 3150
F 0 "R3" V 4593 3150 50  0000 C CNN
F 1 "R" V 4684 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 4730 3150 50  0001 C CNN
F 3 "~" H 4800 3150 50  0001 C CNN
	1    4800 3150
	1    0    0    -1  
$EndComp
Text Label 3750 3350 3    50   ~ 0
LED_3V3_1
Text Label 4350 3350 3    50   ~ 0
LED_3V3_2
Text Label 4950 3350 3    50   ~ 0
LED_3V3_3
Text Label 5550 3350 3    50   ~ 0
LED_3V3_4
$Comp
L power:+3.3V #PWR0101
U 1 1 5F55FD12
P 4650 2850
F 0 "#PWR0101" H 4650 2700 50  0001 C CNN
F 1 "+3.3V" H 4665 3023 50  0000 C CNN
F 2 "" H 4650 2850 50  0001 C CNN
F 3 "" H 4650 2850 50  0001 C CNN
	1    4650 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 1200 2200 1250
Wire Wire Line
	2200 2400 2200 2450
Wire Wire Line
	2200 3550 2200 3600
Wire Wire Line
	2200 4700 2200 4750
Wire Wire Line
	2200 4750 2300 4750
Connection ~ 2200 4750
Wire Wire Line
	2200 4750 2200 4800
Wire Wire Line
	2200 3600 2300 3600
Connection ~ 2200 3600
Wire Wire Line
	2200 3600 2200 3650
Wire Wire Line
	2200 2450 2300 2450
Connection ~ 2200 2450
Wire Wire Line
	2200 2450 2200 2500
Wire Wire Line
	2200 1250 2300 1250
Connection ~ 2200 1250
Wire Wire Line
	2200 1250 2200 1300
Text Label 2300 1250 0    50   ~ 0
LED_CONTROL_1
Text Label 2300 2450 0    50   ~ 0
LED_CONTROL_2
Text Label 2300 3600 0    50   ~ 0
LED_CONTROL_3
Text Label 2300 4750 0    50   ~ 0
LED_CONTROL_4
$Comp
L Device:R R7
U 1 1 5F56B3B1
P 5050 3150
F 0 "R7" H 5120 3196 50  0000 L CNN
F 1 "R" H 5120 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 4980 3150 50  0001 C CNN
F 3 "~" H 5050 3150 50  0001 C CNN
	1    5050 3150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R5
U 1 1 5F56B7D4
P 3850 3150
F 0 "R5" H 3920 3196 50  0000 L CNN
F 1 "R" H 3920 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 3780 3150 50  0001 C CNN
F 3 "~" H 3850 3150 50  0001 C CNN
	1    3850 3150
	-1   0    0    1   
$EndComp
$Comp
L Device:R R6
U 1 1 5F570A0B
P 4450 3150
F 0 "R6" H 4520 3196 50  0000 L CNN
F 1 "R" H 4520 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 4380 3150 50  0001 C CNN
F 3 "~" H 4450 3150 50  0001 C CNN
	1    4450 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	5400 3300 5400 3350
Wire Wire Line
	5400 3350 5650 3350
Wire Wire Line
	5650 3350 5650 3300
Wire Wire Line
	4800 3300 4800 3350
Wire Wire Line
	4800 3350 5050 3350
Wire Wire Line
	5050 3350 5050 3300
Wire Wire Line
	4200 3300 4200 3350
Wire Wire Line
	4200 3350 4450 3350
Wire Wire Line
	4450 3350 4450 3300
Wire Wire Line
	3600 3300 3600 3350
Wire Wire Line
	3600 3350 3850 3350
Wire Wire Line
	3850 3350 3850 3300
Wire Wire Line
	3600 3000 3600 2950
Wire Wire Line
	3600 2950 3850 2950
Wire Wire Line
	5650 2950 5650 3000
Wire Wire Line
	5400 3000 5400 2950
Connection ~ 5400 2950
Wire Wire Line
	5400 2950 5650 2950
Wire Wire Line
	5050 3000 5050 2950
Connection ~ 5050 2950
Wire Wire Line
	5050 2950 5400 2950
Wire Wire Line
	4800 3000 4800 2950
Connection ~ 4800 2950
Wire Wire Line
	4800 2950 5050 2950
Wire Wire Line
	4450 3000 4450 2950
Connection ~ 4450 2950
Wire Wire Line
	4450 2950 4650 2950
Wire Wire Line
	4200 3000 4200 2950
Connection ~ 4200 2950
Wire Wire Line
	4200 2950 4450 2950
Wire Wire Line
	3850 3000 3850 2950
Connection ~ 3850 2950
Wire Wire Line
	3850 2950 4200 2950
$Comp
L Device:R R4
U 1 1 5F552A53
P 5400 3150
F 0 "R4" V 5193 3150 50  0000 C CNN
F 1 "R" V 5284 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 5330 3150 50  0001 C CNN
F 3 "~" H 5400 3150 50  0001 C CNN
	1    5400 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5F56B065
P 5650 3150
F 0 "R8" H 5720 3196 50  0000 L CNN
F 1 "R" H 5720 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_1210_3225Metric_Pad1.42x2.65mm_HandSolder" V 5580 3150 50  0001 C CNN
F 3 "~" H 5650 3150 50  0001 C CNN
	1    5650 3150
	-1   0    0    1   
$EndComp
Wire Wire Line
	4650 2850 4650 2950
Connection ~ 4650 2950
Wire Wire Line
	4650 2950 4800 2950
$Comp
L Components:203556-0407 J4
U 1 1 5F57950C
P 5200 2200
F 0 "J4" H 5600 1635 50  0000 C CNN
F 1 "203556-0407" H 5600 1726 50  0000 C CNN
F 2 "Components:2035560407" H 5850 2300 50  0001 L CNN
F 3 "https://www.molex.com/webdocs/datasheets/pdf/en-us/2035560407_PCB_HEADERS.pdf" H 5850 2200 50  0001 L CNN
F 4 "Headers & Wire Housings PicoClasp Hdr SMT SR Vrt 4Ckt W/FL Au0.38" H 5850 2100 50  0001 L CNN "Description"
F 5 "5.41" H 5850 2000 50  0001 L CNN "Height"
F 6 "538-203556-0407" H 5850 1900 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/203556-0407?qs=F5EMLAvA7IDG5KHyS4NlWw%3D%3D" H 5850 1800 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 5850 1700 50  0001 L CNN "Manufacturer_Name"
F 9 "203556-0407" H 5850 1600 50  0001 L CNN "Manufacturer_Part_Number"
	1    5200 2200
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR0103
U 1 1 5F595452
P 4300 1300
F 0 "#PWR0103" H 4300 1150 50  0001 C CNN
F 1 "+3.3V" H 4315 1473 50  0000 C CNN
F 2 "" H 4300 1300 50  0001 C CNN
F 3 "" H 4300 1300 50  0001 C CNN
	1    4300 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4400 2000 4400 2050
Wire Wire Line
	4400 2050 4300 2050
Connection ~ 4400 2050
Wire Wire Line
	4400 2050 4400 2100
$Comp
L power:+3.3V #PWR0104
U 1 1 5F5AA77F
P 4300 2050
F 0 "#PWR0104" H 4300 1900 50  0001 C CNN
F 1 "+3.3V" H 4315 2223 50  0000 C CNN
F 2 "" H 4300 2050 50  0001 C CNN
F 3 "" H 4300 2050 50  0001 C CNN
	1    4300 2050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4400 1300 4400 1350
Connection ~ 4400 1300
Wire Wire Line
	4400 1300 4300 1300
Wire Wire Line
	4400 1250 4400 1300
Text Label 4400 1150 2    50   ~ 0
LED_CONTROL_1
Text Label 4400 1450 2    50   ~ 0
LED_CONTROL_2
$Comp
L Components:203556-0407 J3
U 1 1 5F578866
P 5200 1450
F 0 "J3" H 5600 885 50  0000 C CNN
F 1 "203556-0407" H 5600 976 50  0000 C CNN
F 2 "Components:2035560407" H 5850 1550 50  0001 L CNN
F 3 "https://www.molex.com/webdocs/datasheets/pdf/en-us/2035560407_PCB_HEADERS.pdf" H 5850 1450 50  0001 L CNN
F 4 "Headers & Wire Housings PicoClasp Hdr SMT SR Vrt 4Ckt W/FL Au0.38" H 5850 1350 50  0001 L CNN "Description"
F 5 "5.41" H 5850 1250 50  0001 L CNN "Height"
F 6 "538-203556-0407" H 5850 1150 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Molex/203556-0407?qs=F5EMLAvA7IDG5KHyS4NlWw%3D%3D" H 5850 1050 50  0001 L CNN "Mouser Price/Stock"
F 8 "Molex" H 5850 950 50  0001 L CNN "Manufacturer_Name"
F 9 "203556-0407" H 5850 850 50  0001 L CNN "Manufacturer_Part_Number"
	1    5200 1450
	-1   0    0    1   
$EndComp
Text Label 1700 4200 0    50   ~ 0
HEATSINK
Text Label 1700 3050 0    50   ~ 0
HEATSINK
Text Label 1700 1900 0    50   ~ 0
HEATSINK
Text Label 1700 700  0    50   ~ 0
HEATSINK
$Comp
L Mechanical:MountingHole H1
U 1 1 5F6B89FE
P 6550 1800
F 0 "H1" H 6650 1846 50  0000 L CNN
F 1 "MountingHole" H 6650 1755 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 6550 1800 50  0001 C CNN
F 3 "~" H 6550 1800 50  0001 C CNN
	1    6550 1800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F6B90A2
P 6550 2050
F 0 "H2" H 6650 2096 50  0000 L CNN
F 1 "MountingHole" H 6650 2005 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 6550 2050 50  0001 C CNN
F 3 "~" H 6550 2050 50  0001 C CNN
	1    6550 2050
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F6B9215
P 6550 2300
F 0 "H3" H 6650 2346 50  0000 L CNN
F 1 "MountingHole" H 6650 2255 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.2mm_M2" H 6550 2300 50  0001 C CNN
F 3 "~" H 6550 2300 50  0001 C CNN
	1    6550 2300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
